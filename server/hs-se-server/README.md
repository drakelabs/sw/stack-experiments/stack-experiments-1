# Server component in Stack Experiment 1: Haskell

This directory contains the HTTP server section of the experimental
Haskell/Purescript application stack.

## Haskell Norms

This should be written with "simple Haskell"; i.e., minimal esoteric language
pragma. It's a "slow English" paradigm, which goes along with the mission of
the project: shareable, working example code as close to real-world as
possible.
