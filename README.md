[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](http://www.gnu.org/licenses/agpl-3.0)

Copyright under the terms of the [Affero GPL License V3](agpl-3.0.md)

# New stack experiment 1

## Intent
The intent of this project is to evaluate, then demonstrate, new stacks made possible with recent combinations of interpreters, compilers, meta-programming cools (e.g., AST generators), build and transpilation systems, and even idioms.

# The No-Limits ADT Experiment

* Haskell generation and potentionally HTTP server.
* Purescript front-end
* Vanilla intermediate code mostly not needed (save for fast native primitives).

## Abstract

This experiment will attempt to create a viable, robust application in reasonable time using lazy evaluation from the "server" (which may not always need to be acting as a traditional HTTP server), and the eager or strict front end of a PureScript UI system. For the purposes of the experiment, we'd like to lean towards one that is not completely bespoke, but that is not Pux or Halogen, either (though those are both fine choices). For the experiment, exploring and demonstrating the "native" features of Purescript running in a client application (browser, extension. or OS/node/electron).

### Haskell "Pragma"

For the experiment, the Haskell will be "simple" Haskell; i.e., no extensions.

### Build and Packaging

The most current possible is to be used. This is somewhat controversial. This _does_ mean no `bower` and no `Cabal`. Nix for its packaging capabilities may be considered, but not for in its full-OS form.

## Application structure

The structure is very simple client/server, using idiomatic HTTP (_not_
esoteric variants from a certain very large advertising/search provider).

The latest browser APIs are fair game, as long as (again) HTTP is used as a
transport protocol and not as a dumb wire protocol.

### Example "fair game" tech

* `localstorage` (or any abstraction thereof)
* `webworkers`
* `serviceworkers`
* `glsl/WebGL`
  * Static types created for (but not necessarily used with) the above).

### Non-usable web tech

* While neat and all, webassembly-related tech defeats the purpose of the
  experiment (think of it as the largest imaginable escape hatch).


_From each according to their ability, to each according to their needs._
